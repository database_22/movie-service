package movie

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"movie-service/context"
	"movie-service/db"
	"movie-service/models"
	"movie-service/utils"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type MovieService struct {
	DatabaseRepository db.RepositoryInterface
}

type Interface interface {
	GetMovie(ctx *context.AppContext, w http.ResponseWriter, r *http.Request)
	GetMovieByMood(ctx *context.AppContext, w http.ResponseWriter, r *http.Request)
	GetMovieOfTheDay(ctx *context.AppContext, w http.ResponseWriter, r *http.Request)
	GetRandomMovie(ctx *context.AppContext, w http.ResponseWriter, r *http.Request)
	GetUndiscoveredMovie(ctx *context.AppContext, w http.ResponseWriter, r *http.Request)
	GetMovies(ctx *context.AppContext, w http.ResponseWriter, r *http.Request)
	FlushMovieByMood(ctx *context.AppContext, w http.ResponseWriter, r *http.Request)
}

func NewMovieService(repository db.RepositoryInterface) Interface {
	return &MovieService{
		repository,
	}
}

func (m *MovieService) GetMovieByMood(ctx *context.AppContext, w http.ResponseWriter, r *http.Request) {

	authId := r.Header.Get("Authorization")
	if len(authId) == 0 {
		http.Error(w, "No auth id", http.StatusBadRequest)
		return
	}

	var inputBody models.MLModel
	err := json.NewDecoder(r.Body).Decode(&inputBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	requestBody, err := json.Marshal(inputBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	responseMLData, err := utils.DoRequest("POST", "http://51.250.21.146:8000/movie_by_the_mood", requestBody, authId, "application/json")
	var responseTemp models.MLResponseModel
	fmt.Println(string(responseMLData))
	err = json.Unmarshal(responseMLData, &responseTemp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var movies []models.Movie
	for i := 0; i < len(responseTemp.MovieIds); i++ {
		//var tempMovie models.Movie

		movie, err := m.DatabaseRepository.GetMovie(strconv.Itoa(responseTemp.MovieIds[i]))
		if err != nil {
			log.Fatal(err)
		}
		movies = append(movies, *movie)
	}

	jsn, err := json.Marshal(movies)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	utils.RespBytes(w, jsn, http.StatusOK)
}

func (m *MovieService) GetMovieOfTheDay(ctx *context.AppContext, w http.ResponseWriter, r *http.Request) {
	t := time.Now()
	currentDate := t.Format("2006-01-02")

	var returnMovie models.Movie
	currentMovie, err := m.DatabaseRepository.GetMovieOfTheDay()
	newMovie, err := m.DatabaseRepository.GetMovieCustomized(utils.MovieOfTheDay)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if currentMovie != nil {
		temp := strings.Split(currentMovie.UpdatedTime, "T")
		fmt.Println("date", temp[0])
		fmt.Println("currentDate", currentDate)
		if temp[0] != currentDate {
			err = m.DatabaseRepository.UpdateMovieOfTheDay(currentMovie.Id, newMovie.Id)
			if err != nil {
				utils.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			returnMovie = *newMovie
		} else {
			movie, err := m.DatabaseRepository.GetMovie(currentMovie.Id)
			if err != nil {
				utils.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			returnMovie = *movie
		}
	} else {
		err = m.DatabaseRepository.UpdateMovieOfTheDay("", newMovie.Id)
		if err != nil {
			utils.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		returnMovie = *newMovie
	}

	jsn, err := json.Marshal(returnMovie)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	utils.RespBytes(w, jsn, http.StatusOK)
}

func (m *MovieService) GetRandomMovie(ctx *context.AppContext, w http.ResponseWriter, r *http.Request) {
	movie, err := m.DatabaseRepository.GetMovieCustomized(utils.RandomMovie)
	if err != nil {
		log.Fatal(err)
		return
	}

	jsn, err := json.Marshal(movie)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	utils.RespBytes(w, jsn, http.StatusOK)
}

func (m *MovieService) GetUndiscoveredMovie(ctx *context.AppContext, w http.ResponseWriter, r *http.Request) {
	movie, err := m.DatabaseRepository.GetMovieCustomized(utils.UndiscoveredMovie)
	if err != nil {
		log.Fatal(err)
		return
	}

	jsn, err := json.Marshal(movie)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	utils.RespBytes(w, jsn, http.StatusOK)
}

func (m *MovieService) GetMovie(ctx *context.AppContext, w http.ResponseWriter, r *http.Request) {
	pathParams := mux.Vars(r)
	id := pathParams["id"]
	id = strings.Trim(strings.ToLower(id), " ")

	var idInt int
	idInt, err := strconv.Atoi(id)

	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	movie, err := m.DatabaseRepository.GetMovie(fmt.Sprint(idInt))
	if err != nil {
		log.Fatal(err)
		return
	}

	jsn, err := json.Marshal(movie)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	utils.RespBytes(w, jsn, http.StatusOK)
}

func (m *MovieService) GetMovies(ctx *context.AppContext, w http.ResponseWriter, r *http.Request) {

	pathParams := mux.Vars(r)
	page := pathParams["page"]
	page = strings.Trim(strings.ToLower(page), " ")

	var (
		movie   []models.Movie
		err     error
		pageInt int
	)
	pageInt, err = strconv.Atoi(page)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	movie, err = m.DatabaseRepository.GetMovies(pageInt)
	if err != nil {
		log.Fatal(err)
		return
	}

	jsn, err := json.Marshal(movie)
	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	utils.RespBytes(w, jsn, http.StatusOK)
}

func (m *MovieService) FlushMovieByMood(ctx *context.AppContext, w http.ResponseWriter, r *http.Request) {
	authId := r.Header.Get("Authorization")
	if len(authId) == 0 {
		http.Error(w, "No auth id", http.StatusBadRequest)
		return
	}

	_, err := utils.DoRequest("POST", "http://51.250.21.146:8000/flush_cache", []byte(""), authId, "application/json")

	if err != nil {
		utils.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	utils.RespBytes(w, []byte(""), http.StatusOK)
}
