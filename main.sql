create table CAST
(
    CA_MOVIE_ID     NUMBER(38),
    CA_CAST_ID      NUMBER(38),
    CA_CHARACTER    VARCHAR2(512),
    CA_CREDIT_ID    VARCHAR2(26),
    CA_GENDER       NUMBER(38),
    CA_ID           NUMBER(38),
    CA_NAME         VARCHAR2(512),
    CA_ORDER        NUMBER(38),
    CA_PROFILE_PATH VARCHAR2(128)
);

create table COLLECTION
(
    CL_MOVIE_ID      NUMBER(38),
    CL_ID            NUMBER(38),
    CL_NAME          VARCHAR2(512),
    CL_POSTER_PATH   VARCHAR2(128),
    CL_BACKDROP_PATH VARCHAR2(128)
);

create table COMPANIES
(
    CM_MOVIE_ID NUMBER(38),
    CM_ID       NUMBER(38),
    CM_NAME     VARCHAR2(512)
);

create table COUNTRIES
(
    CN_MOVIE_ID   NUMBER(38),
    CN_ISO_3166_1 VARCHAR2(4),
    CN_NAME       VARCHAR2(256)
);

create table CREW
(
    CR_MOVIE_ID     NUMBER(38),
    CR_CREDIT_ID    VARCHAR2(26),
    CR_DEPARTMENT   VARCHAR2(26),
    CR_GENDER       NUMBER(38),
    CR_ID           NUMBER(38),
    CR_JOB          VARCHAR2(128),
    CR_NAME         VARCHAR2(512),
    CR_PROFILE_PATH VARCHAR2(128)
);

create table GENRES
(
    GE_MOVIE_ID NUMBER(38),
    GE_ID       NUMBER(38),
    GE_NAME     VARCHAR2(256)
);

create table KEYWORDS
(
    KW_MOVIE_ID NUMBER(38),
    KW_ID       NUMBER(38),
    KW_NAME     VARCHAR2(512)
);

create table LINKS
(
    LI_MOVIE_ID    VARCHAR2(256),
    LI_IMDB_ID     VARCHAR2(256),
    LI_INTERNAL_ID VARCHAR2(256)
);

create table METADATA
(
    MD_MOVIE_ID     NUMBER(38),
    MD_ADULT        VARCHAR2(26),
    MD_BUDGET       NUMBER(38),
    MD_HOMEPAGE     VARCHAR2(256),
    MD_IMDB_ID      VARCHAR2(26),
    MD_ORIG_LANG    VARCHAR2(26),
    MD_ORIG_TITLE   VARCHAR2(256),
    MD_OVERVIEW     VARCHAR2(4000),
    MD_POPULARITY   NUMBER(38, 6),
    MD_POSTER_PATH  VARCHAR2(128),
    MD_RELEASE_DATE DATE,
    MD_REVENUE      NUMBER(38),
    MD_RUNTIME      NUMBER(38),
    MD_STATUS       VARCHAR2(26),
    MD_TAGLINE      VARCHAR2(512),
    MD_TITLE        VARCHAR2(256),
    MD_VIDEO        VARCHAR2(26),
    MD_VOTE_AVG     NUMBER(38, 1),
    MD_VOTE_CNT     NUMBER(38)
);

create table RATINGS
(
    RA_MOVIE_ID  NUMBER(38),
    RA_USER_ID   NUMBER(38),
    RA_RATING    NUMBER(38, 1),
    RA_TIMESTAMP NUMBER(38)
);

create table SPOKENLANGUAGES
(
    SL_MOVIE_ID  NUMBER(38),
    SL_ISO_639_1 VARCHAR2(26),
    SL_NAME      VARCHAR2(256)
);

create table MOVIE_OF_THE_DAY_HISTORY
(
    MOVIE_ID     NUMBER not null,
    CREATED_TIME DATE,
    UPDATED_TIME DATE,
    CREATED_BY   VARCHAR2(255),
    UPDATED_BY   VARCHAR2(255)
)

--package specifitaion
CREATE OR REPLACE PACKAGE movie_package AS
    TYPE MovieShortRecordTyp IS RECORD (
                                           movie_id NUMBER(38),
                                           title VARCHAR2(256),
                                           overview VARCHAR2(4000),
                                           language VARCHAR2(26),
                                           poster_url VARCHAR2(128),
                                           rating NUMBER(38,1),
                                           release_date DATE); -- short movie description
    TYPE MovieFullRecordType IS RECORD (
                                           MD_MOVIE_ID NUMBER(38),
                                           MD_ADULT        VARCHAR2(26),
                                           MD_BUDGET       NUMBER(38),
                                           MD_HOMEPAGE     VARCHAR2(256),
                                           MD_IMDB_ID      VARCHAR2(26),
                                           MD_ORIG_LANG    VARCHAR2(26),
                                           MD_ORIG_TITLE   VARCHAR2(256),
                                           MD_OVERVIEW     VARCHAR2(4000),
                                           MD_POPULARITY   NUMBER(38, 6),
                                           MD_POSTER_PATH  VARCHAR2(128),
                                           MD_RELEASE_DATE DATE,
                                           MD_REVENUE      NUMBER(38),
                                           MD_RUNTIME      NUMBER(38),
                                           MD_STATUS       VARCHAR2(26),
                                           MD_TAGLINE      VARCHAR2(512),
                                           MD_TITLE        VARCHAR2(256),
                                           MD_VIDEO        VARCHAR2(26),
                                           MD_VOTE_AVG     NUMBER(38, 1),
                                           MD_VOTE_CNT     NUMBER(38));
    CURSOR movie_short_description(movie_id in number) RETURN MovieShortRecordTyp; --cursor to get short description of movie
    CURSOR movie_full_description(movie_id in number) RETURN MovieFullRecordType; --cursor to get full description of movie

    FUNCTION fetch_movie_short (movie_id NUMBER) return MovieShortRecordTyp;
    FUNCTION fetch_movie_full (movie_id NUMBER) return MovieFullRecordType;
    FUNCTION fetch_movie_as_row (movie_id NUMBER) return METADATA%rowtype;
    PROCEDURE create_movie (movie_id NUMBER, isAdult VARCHAR2, budget NUMBER, homepage VARCHAR2, imdb_id VARCHAR2, orig_lang VARCHAR2, orig_title VARCHAR2, overview VARCHAR2, popularity NUMBER, poster VARCHAR2, release_date DATE, revenue NUMBER, runtime NUMBER, status VARCHAR2, tagline VARCHAR2, title VARCHAR2, video VARCHAR2, vote_avg NUMBER, vote_cnt NUMBER); -- create movie
    PROCEDURE delete_movie (movie_id NUMBER); -- removing movie
END movie_package;

CREATE OR REPLACE PACKAGE BODY movie_package AS
    cursor movie_short_description(movie_id in number) RETURN MovieShortRecordTyp IS
        SELECT MD_MOVIE_ID, MD_TITLE, MD_OVERVIEW, MD_ORIG_LANG, MD_POSTER_PATH, MD_VOTE_AVG, MD_RELEASE_DATE FROM METADATA where MD_MOVIE_ID=movie_id;
    cursor movie_full_description(movie_id in number) RETURN MovieFullRecordType IS
        SELECT * FROM METADATA where MD_MOVIE_ID=movie_id;

    PROCEDURE create_movie (movie_id NUMBER, isAdult VARCHAR2, budget NUMBER, homepage VARCHAR2, imdb_id VARCHAR2, orig_lang VARCHAR2, orig_title VARCHAR2, overview VARCHAR2, popularity NUMBER, poster VARCHAR2, release_date DATE, revenue NUMBER, runtime NUMBER, status VARCHAR2, tagline VARCHAR2, title VARCHAR2, video VARCHAR2, vote_avg NUMBER, vote_cnt NUMBER) IS
    BEGIN
        INSERT INTO METADATA VALUES (movie_id, isAdult, budget, homepage, imdb_id, orig_lang, orig_title, overview, popularity, poster, release_date, revenue, runtime, status, tagline, title, video, vote_avg, vote_cnt);
    END create_movie;

    PROCEDURE delete_movie (movie_id NUMBER) IS
    BEGIN
        DELETE FROM METADATA WHERE MD_MOVIE_ID = movie_id;
    END delete_movie;

    function fetch_movie_short(movie_id Number) RETURN MovieShortRecordTyp IS
        newShortMovie MovieShortRecordTyp;
    BEGIN
        OPEN movie_short_description(movie_id);
        LOOP
            FETCH movie_short_description INTO newShortMovie;
            EXIT WHEN movie_short_description%notfound;
        END LOOP;
        CLOSE movie_short_description;
        RETURN newShortMovie;
    END fetch_movie_short;

    FUNCTION fetch_movie_full (movie_id NUMBER) return MovieFullRecordType IS
        newFullMovie MovieFullRecordType;
    BEGIN
        OPEN movie_full_description(movie_id);
        LOOP
            FETCH movie_full_description INTO newFullMovie;
            EXIT WHEN movie_full_description%notfound;
        END LOOP;
        CLOSE movie_full_description;
        RETURN newFullMovie;
    END fetch_movie_full;

    FUNCTION fetch_movie_as_row (movie_id NUMBER) return METADATA%rowtype IS
        movie METADATA%rowtype;
    BEGIN
        SELECT * INTO movie FROM METADATA WHERE MD_MOVIE_ID=movie_id;
        return movie;
    END fetch_movie_as_row;
END movie_package;

-- call
DECLARE
    c MOVIE_PACKAGE.MovieFullRecordType;
    query     VARCHAR2(1000);
BEGIN
    c := MOVIE_PACKAGE.fetch_movie_full(31357);
    dbms_output.put_line(c);
end;

create or replace type t_table as table of MOVIE_PACKAGE.MovieShortRecordTyp;
/

create or replace function return_table return t_table as
    v_ret   t_table;
begin

    --
    -- Call constructor to create the returned
    -- variable:
    --
    v_ret  := t_table();

    --
    -- Add one record after another to the returned table.
    -- Note: the »table« must be extended before adding
    -- another record:
    --
    v_ret.extend; v_ret(v_ret.count) := movie_package.MovieShortRecordTyp(1, 'one'  );
    v_ret.extend; v_ret(v_ret.count) := t_record(2, 'two'  );
    v_ret.extend; v_ret(v_ret.count) := t_record(3, 'three');

    --
    -- Return the record:
    --
    return v_ret;

end return_table;
/
-- SELECT * FROM METADATA where MD_MOVIE_ID=31357;

SELECT *
FROM   METADATA
ORDER BY MD_MOVIE_ID
OFFSET 20 ROWS FETCH NEXT 30 ROWS ONLY;

-- select md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date
--     			from (
--     			select md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date,
--      				row_number() over
--        				(order by MD_VOTE_AVG DESC ) rn
--     				from METADATA)
--     where rn between 0 and 10
--     order by rn;

ALTER TABLE METADATA ENABLE ALL TRIGGERS;

--
CREATE OR REPLACE TRIGGER add_fields_after_insert
    BEFORE INSERT
    ON MOVIE_OF_THE_DAY
    FOR EACH ROW
DECLARE
    v_username varchar2(10);
    v_create_date date;
BEGIN
    -- Найти персону username, осуществляющего INSERT в таблицу
    SELECT CURRENT_DATE INTO v_create_date FROM DUAL;

    SELECT user INTO v_username
    FROM dual;

    :new.created_time := v_create_date;
    :new.created_by := v_username;
    :new.updated_time := SYSDATE;
    :new.updated_by := v_username;
END;

--
--
create trigger CHECK_ID_ON_INSERT
    before insert
    on MOVIE_OF_THE_DAY
    for each row
DECLARE
    v_movie_id NUMBER;
    v_temp NUMBER;
BEGIN
    v_temp := :new.movie_id;
    v_movie_id := 0;

    SELECT MD_MOVIE_ID
    INTO v_movie_id
    FROM METADATA
    WHERE MD_MOVIE_ID = v_temp;

    if v_movie_id = 0 then
        raise_application_error(-20000, 'Given id do not exist in METADATA table!');
    end if;
    --     if v_movie_id != :new.MOVIE_ID then
--         raise_application_error(100, 'Given id do not exist in METADATA table!');
--     end if;
END;

--
CREATE OR REPLACE TRIGGER dump_history_after_drop
    AFTER DELETE
    ON MOVIE_OF_THE_DAY
    FOR EACH ROW
DECLARE
    v_username varchar2(10);
BEGIN
    SELECT user INTO v_username
    FROM dual;

    INSERT INTO MOVIE_OF_THE_DAY_HISTORY
    (movie_id, created_time, updated_time, created_by, updated_by)
    VALUES
        (:old.movie_id, :old.created_time, SYSDATE, :old.created_by, v_username);
END;

--
CREATE OR REPLACE TRIGGER add_fields_before_update
    BEFORE UPDATE
    ON MOVIE_OF_THE_DAY
    FOR EACH ROW
DECLARE
    v_username varchar2(10);
BEGIN
    SELECT user INTO v_username
    FROM dual;

    :new.updated_time := SYSDATE;
    :new.updated_by := v_username;
END;

-- check function
INSERT INTO ARS.MOVIE_OF_THE_DAY (MOVIE_ID) VALUES (862);

DELETE FROM MOVIE_OF_THE_DAY WHERE MOVIE_ID = '862';

UPDATE MOVIE_OF_THE_DAY
SET MOVIE_ID = 8844
WHERE MOVIE_ID = 862;



-- movieOfTheDay package
CREATE OR REPLACE PACKAGE movie_of_the_day_package AS
    TYPE MovieOfTheDay IS RECORD (
                                     movie_id NUMBER(38),
                                     created_time DATE,
                                     updated_time DATE,
                                     CREATED_BY VARCHAR2(255),
                                     UPDATED_BY VARCHAR2(128));
    CURSOR movie_of_the_day(movie_id in number) RETURN MovieOfTheDay;

    FUNCTION fetch_movie_of_the_day (movie_id NUMBER) return MovieOfTheDay;
    PROCEDURE create_movie_of_the_day (movie_id NUMBER, created_time DATE, updated_time DATE, CREATED_BY VARCHAR2, UPDATED_BY VARCHAR2);
    FUNCTION fetch_movie_as_row (movie_id NUMBER) return MOVIE_OF_THE_DAY%rowtype;
    PROCEDURE delete_movie_of_the_day (movie_id NUMBER);
    PROCEDURE update_movie_of_the_day (old_movie_id NUMBER, new_movie_id NUMBER);
END movie_of_the_day_package;

CREATE OR REPLACE PACKAGE BODY movie_of_the_day_package AS
    CURSOR movie_of_the_day(movie_id in number) RETURN MovieOfTheDay IS
        SELECT * FROM MOVIE_OF_THE_DAY WHERE MOVIE_ID = movie_id;

    function fetch_movie_of_the_day(movie_id Number) RETURN MovieOfTheDay IS
        newMovie MovieOfTheDay;
    BEGIN
        OPEN movie_of_the_day(movie_id);
        LOOP
            FETCH movie_of_the_day INTO newMovie;
            EXIT WHEN movie_of_the_day%notfound;
        END LOOP;
        CLOSE movie_of_the_day;
        RETURN newMovie;
    END fetch_movie_of_the_day;

    FUNCTION fetch_movie_as_row (movie_id NUMBER) return MOVIE_OF_THE_DAY%rowtype IS
        movie MOVIE_OF_THE_DAY%rowtype;
    BEGIN
        SELECT * INTO movie FROM MOVIE_OF_THE_DAY WHERE MOVIE_ID=movie_id;
        return movie;
    END fetch_movie_as_row;

    PROCEDURE create_movie_of_the_day(movie_id NUMBER, created_time DATE, updated_time DATE, created_by VARCHAR2, updated_by VARCHAR2) IS
    BEGIN
        INSERT INTO MOVIE_OF_THE_DAY VALUES  (movie_id, created_time, updated_time, created_by, updated_by);
    END create_movie_of_the_day;

    PROCEDURE delete_movie_of_the_day(movie_id NUMBER) IS
    BEGIN
        DELETE FROM MOVIE_OF_THE_DAY where MOVIE_ID=movie_id;
    END delete_movie_of_the_day;

    PROCEDURE update_movie_of_the_day (old_movie_id NUMBER, new_movie_id NUMBER) IS
    BEGIN
        UPDATE MOVIE_OF_THE_DAY
        SET MOVIE_ID=new_movie_id
        WHERE MOVIE_ID=old_movie_id;
    END;
END movie_of_the_day_package;

-- test package
DECLARE
    returnRow MOVIE_OF_THE_DAY%rowtype;
    returnRecord MOVIE_OF_THE_DAY_PACKAGE.MovieOfTheDay;
BEGIN
    returnRow := MOVIE_OF_THE_DAY_PACKAGE.FETCH_MOVIE_AS_ROW('670');
    returnRecord := MOVIE_OF_THE_DAY_PACKAGE.fetch_movie_of_the_day('670');
    DBMS_OUTPUT.PUT_LINE( returnRow.MOVIE_ID );
    DBMS_OUTPUT.PUT_LINE( returnRecord.MOVIE_ID );

    movie_of_the_day_package.create_movie_of_the_day('123', SYSDATE, SYSDATE, 'ars', 'ars');
    movie_of_the_day_package.update_movie_of_the_day('123', '777');
    movie_of_the_day_package.delete_movie_of_the_day('777');
end;
/

