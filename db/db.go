package db

import (
	"database/sql"
	"fmt"
	_ "github.com/godror/godror"
	"log"
	"movie-service/context"
	"movie-service/models"
	"movie-service/utils"
	"time"
)

func ConnectDB(dataSource string) (*sql.DB, error) {
	db, err := sql.Open("godror", dataSource)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(20)
	db.SetConnMaxLifetime(time.Minute * 5)
	return db, nil
}

type Repository struct {
	AppContext *context.AppContext
}

func (r *Repository) UpdateMovieOfTheDay(oldId, newId string) error {
	queryUpdate := `
	UPDATE MOVIE_OF_THE_DAY
   	SET MOVIE_ID = '%v'
	WHERE MOVIE_ID = '%v'`

	queryInsert := `
	INSERT INTO MOVIE_OF_THE_DAY (MOVIE_ID)
	VALUES ('%v')`

	var (
		stmt *sql.Stmt
		err  error
	)
	if len(oldId) > 0 {
		stmt, err = r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Prepare(fmt.Sprintf(queryUpdate, newId, oldId))
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
	} else {
		stmt, err = r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Prepare(fmt.Sprintf(queryInsert, newId))
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
	}

	defer func() {
		if e := stmt.Close(); e != nil {
			log.Println(e)
		}
	}()

	_, err = stmt.Query()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	return nil
}

func (r Repository) GetMovieOfTheDay() (*models.MovieOfTheDay, error) {
	query := `SELECT * FROM MOVIE_OF_THE_DAY FETCH FIRST 1 ROW ONLY`
	//rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(fmt.Sprintf(query, id))
	//if err != nil {
	//	return nil, err
	stmt, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Prepare(query)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	defer func() {
		if e := stmt.Close(); e != nil {
			log.Println(e)
		}
	}()

	rows, err := stmt.Query()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	var movies []models.MovieOfTheDay
	for rows.Next() {
		var r models.MovieOfTheDay
		err = rows.Scan(&r.Id, &r.CreatedTime, &r.UpdatedTime, &r.CreatedBy, &r.UpdatedBy)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	if len(movies) == 0 {
		return nil, nil
	}
	return &movies[0], nil
}

//func (r Repository) GetMovieTest(id string) (*models.Movie, error) {
//	fmt.Println(id)
//	query := `
//DECLARE
//    c METADATA%rowtype;
//BEGIN
//    c := MOVIE_PACKAGE.FETCH_MOVIE_AS_ROW(334543);
//    dbms_output.put_line(c.MD_ORIG_TITLE);
//END;`
//	//rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(fmt.Sprintf(query, id))
//	//if err != nil {
//	//	return nil, err
//	fmt.Println(query)
//	stmt, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Prepare(query)
//	if err != nil {
//		fmt.Println(err.Error())
//		return nil, err
//	}
//	defer func() {
//		if e := stmt.Close(); e != nil {
//			log.Println(e)
//		}
//	}()
//
//	rows, err := stmt.Query()
//	if err != nil {
//		fmt.Println(err.Error())
//		return nil, err
//	}
//
//	var movies []models.Movie
//	for rows.Next() {
//		var r models.Movie
//		err = rows.Scan(&r.Id)
//		if err != nil {
//			log.Fatalf("Scan: %v", err)
//			return nil, err
//		}
//		movies = append(movies, r)
//	}
//	if len(movies) == 0 {
//		return nil, nil
//	}
//	return &movies[0], nil
//}

type RepositoryInterface interface {
	GetMovie(id string) (*models.Movie, error)
	GetMovies(page int) ([]models.Movie, error)
	GetMovieOfTheDay() (*models.MovieOfTheDay, error)
	GetMovieCustomized(operationType string) (*models.Movie, error)
	UpdateMovieOfTheDay(oldId, newId string) error
	//GetMovieTest(id string) (*models.Movie, error)
}

func NewRepository(appContext *context.AppContext) RepositoryInterface {
	return &Repository{AppContext: appContext}
}

//asdsadsadasdad
func (r Repository) GetMovie(id string) (*models.Movie, error) {
	fmt.Println(id)
	query := `SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date 
FROM Metadata 
WHERE md_movie_id = '%v'`
	//rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(fmt.Sprintf(query, id))
	//if err != nil {
	//	return nil, err
	fmt.Println(fmt.Sprintf(query, id))
	stmt, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Prepare(fmt.Sprintf(query, id))
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	defer func() {
		if e := stmt.Close(); e != nil {
			log.Println(e)
		}
	}()

	rows, err := stmt.Query()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	if len(movies) == 0 {
		return nil, nil
	}
	return &movies[0], nil
}

//TODO: make normal pagination, current is 10 movies on page
func (r Repository) GetMovies(page int) ([]models.Movie, error) {
	lowerBound := 1
	upperBound := 10
	if page > 1 {
		lowerBound, upperBound = page*10-9, page*10
	}
	query := `	select md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date
    			from (
    			select md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date,
     				row_number() over
       				(order by MD_VOTE_AVG DESC, MD_VOTE_CNT DESC ) rn
    				from METADATA
    			    WHERE MD_VOTE_CNT > 1000)
    where rn between '%v' and '%v'
    order by rn`
	stmt, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Prepare(fmt.Sprintf(query, lowerBound, upperBound))
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	defer func() {
		if e := stmt.Close(); e != nil {
			log.Println(e)
		}
	}()

	rows, err := stmt.Query()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	if len(movies) == 0 {
		return nil, nil
	}
	return movies, nil
}

func (r Repository) GetMovieCustomized(operationType string) (*models.Movie, error) {
	var movie *models.Movie
	var err error
	switch operationType {
	case utils.RandomMovie:
		movie, err = r.getRandomMovie()
	case utils.UndiscoveredMovie:
		movie, err = r.getUndiscoveredMovie()
	case utils.MovieOfTheDay:
		movie, err = r.getMovieOfTheDay()
	}
	if err != nil {
		return nil, err
	}
	return movie, err
}

func (r Repository) getMovieOfTheDay() (*models.Movie, error) {
	query := `	SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date 
				FROM METADATA 
				WHERE md_vote_avg >= 8 AND md_vote_cnt >= 100
				ORDER BY DBMS_RANDOM.RANDOM()
				fetch first 1 rows only`
	//rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(query)
	//if err != nil {
	//	return nil, err
	//}

	stmt, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer func() {
		if e := stmt.Close(); e != nil {
			log.Println(e)
		}
	}()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer func() {
		if e := rows.Close(); e != nil {
			log.Println(e)
		}
	}()

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	return &movies[0], nil
}

func (r Repository) getRandomMovie() (*models.Movie, error) {
	query := `	SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date 
				FROM Metadata 
				ORDER BY DBMS_RANDOM.RANDOM()
				fetch first 1 rows only`
	rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(query)
	if err != nil {
		return nil, err
	}

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	return &movies[0], nil
}

func (r Repository) getUndiscoveredMovie() (*models.Movie, error) {
	query := `	SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date 
				FROM Metadata 
				WHERE md_vote_avg >= 6 AND md_vote_avg <= 8 AND md_vote_cnt >= 20
				ORDER BY DBMS_RANDOM.RANDOM()
				fetch first 1 rows only`
	rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(query)
	if err != nil {
		return nil, err
	}

	fmt.Println(&rows)
	var movies []models.Movie

	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	return &movies[0], nil
}
