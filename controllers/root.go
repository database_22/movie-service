package controllers

import (
	"fmt"
	"movie-service/context"
	"net/http"
)

func RootController(_ *context.AppContext, response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "text/plain")
	_, _ = fmt.Fprintf(response, "MovieAPI")
}
