package controllers

import (
	"movie-service/context"
	"movie-service/db"
	"movie-service/services/movie"
	"net/http"
)

func GetMovies(ctx *context.AppContext, writer http.ResponseWriter, request *http.Request) {
	databaseRepository := db.NewRepository(ctx)
	service := movie.NewMovieService(databaseRepository)
	service.GetMovies(ctx, writer, request)
}

func GetMovieByMood(ctx *context.AppContext, writer http.ResponseWriter, request *http.Request) {
	databaseRepository := db.NewRepository(ctx)
	service := movie.NewMovieService(databaseRepository)
	service.GetMovieByMood(ctx, writer, request)
}

func FlushMovieByMood(ctx *context.AppContext, writer http.ResponseWriter, request *http.Request) {
	databaseRepository := db.NewRepository(ctx)
	service := movie.NewMovieService(databaseRepository)
	service.FlushMovieByMood(ctx, writer, request)
}

func GetMovieOfTheDay(ctx *context.AppContext, writer http.ResponseWriter, request *http.Request) {
	databaseRepository := db.NewRepository(ctx)
	service := movie.NewMovieService(databaseRepository)
	service.GetMovieOfTheDay(ctx, writer, request)
}

func GetRandomMovie(ctx *context.AppContext, writer http.ResponseWriter, request *http.Request) {
	databaseRepository := db.NewRepository(ctx)
	service := movie.NewMovieService(databaseRepository)
	service.GetRandomMovie(ctx, writer, request)
}

func GetUndiscoveredMovie(ctx *context.AppContext, writer http.ResponseWriter, request *http.Request) {
	databaseRepository := db.NewRepository(ctx)
	service := movie.NewMovieService(databaseRepository)
	service.GetUndiscoveredMovie(ctx, writer, request)
}

func GetMovie(appContext *context.AppContext, w http.ResponseWriter, r *http.Request) {
	databaseRepository := db.NewRepository(appContext)
	service := movie.NewMovieService(databaseRepository)
	service.GetMovie(appContext, w, r)
}
