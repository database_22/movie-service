package main

import (
	"github.com/rs/cors"
	"log"
	"movie-service/context"
	"movie-service/db"
	"movie-service/routers"
	"net/http"
)

//TODO: flag of env parse
var (
	dbPath = `user="ars" password="1234" connectString="localhost:1521/orcl"`
	port   = ":10000"
)

func main() {
	movieDb, err := db.ConnectDB(dbPath)
	if err != nil {
		log.Fatal("cannot initialize database: " + err.Error())
	}
	databaseContext := context.NewDatabaseContext(context.Database{DatabaseHandler: movieDb})
	appContext := context.NewAppContext(databaseContext)

	router, err := routers.InitializeRouter(appContext)
	if err != nil {
		log.Fatal("cannot initialize routers: " + err.Error())
	}
	// cors.AllowAll() setup the middleware with default options being
	// all origins accepted with simple methods (GET, POST). See
	// documentation below for more options.
	handler := cors.AllowAll().Handler(router)

	log.Fatal(http.ListenAndServe(port, handler))
}
