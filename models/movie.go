package models

type Movie struct {
	Id           string `json:"id,omitempty" db:"md_movie_id"`
	Title        string `json:"title,omitempty" db:"md_title"`
	Overview     string `json:"overview,omitempty" db:"md_overview"`
	Language     string `json:"language,omitempty" db:"md_orig_lang"`
	PosterUrl    string `json:"poster_url,omitempty" db:"md_poster_path"`
	Rating       string `json:"rating,omitempty" db:"md_vote_avg"`
	ReleasedDate string `json:"released_date,omitempty" db:"md_release_date"`
}

type MovieOfTheDay struct {
	Id          string `db:"movie_id"`
	CreatedTime string `db:"created_time"`
	UpdatedTime string `db:"updated_time"`
	CreatedBy   string `db:"created_by"`
	UpdatedBy   string `db:"updated_by"`
}

type MLModel struct {
	LikedMovies    []int `json:"liked_movies"`
	DislikedMovies []int `json:"disliked_movies"`
	NeitherMovies  []int `json:"neither_movies"`
}

type MLResponseModel struct {
	ResponseType string `json:"response_type"`
	MovieIds     []int  `json:"movie_ids"`
}
