package utils

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

type Message struct {
	Message string `json:"message"`
}

func Error(w http.ResponseWriter, error string, code int) {
	Resp(w, error, code)
}

func Resp(w http.ResponseWriter, str string, code int) {
	var m Message
	m.Message = str
	jsn, _ := json.Marshal(m)
	RespBytes(w, jsn, code)
}

func RespBytes(w http.ResponseWriter, bytes []byte, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	_, _ = w.Write(bytes)
}

func DoRequest(method string, url string, inputBody []byte,
	authorization string, contentType string) ([]byte, error) {

	timeout := 5 * time.Second
	client := http.Client{
		Timeout: timeout,
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(inputBody))
	if err != nil {
		return nil, err
	}

	req.Header.Set("user-id", authorization)
	req.Header.Set("Content-Type", contentType)
	req.Close = true

	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func() {
		response.Body.Close()
	}()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		return nil, errors.New(response.Status + ":" + string(body))
	}

	return body, nil
}
